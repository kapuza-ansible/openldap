# OpenLdap role
## Install
### Install python (ubuntu)
For use ansible, you need install python2.7 and python-apt on remote server.
```bash
ansible xen -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt;"
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash
allow_world_readable_tmpfiles=True

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### download openldap role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/openldap.git
  name: openldap
EOF
echo "roles/openldap" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## OpenLdap
Install and setup openldap server.
```bash
cat << 'EOF' > openldap.yml
---
- hosts:
    ldap
  become: yes
  become_user: root
  tasks:
    - name: Openldap
      include_role:
        name: openldap
EOF

ansible-playbook ./openldap.yml
```
